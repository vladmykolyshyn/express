import * as groupsService from './groups.service';
import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupCreateRequest } from './types/group-create-request.interface';
import { IGroupUpdateRequest } from './types/group-update-request.interface';

export const getAllGroups = async (request: Request, response: Response) => {
  const groups = await groupsService.getAllGroups();
  response.status(200).json(groups);
};

export const getGroupById = async (
  request: Request<{ id: number }>,
  response: Response
) => {
  const { id } = request.params;
  const groupWithStudents = await groupsService.getGroupById(id);
  response.status(200).json(groupWithStudents);
};

export const createGroup = async (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response
) => {
  const group = await groupsService.createGroup(request.body);
  response.status(201).json(group);
};

export const updateGroupById = async (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response
) => {
  const { id } = request.params;
  const group = await groupsService.updateGroupById(id, request.body);
  response.status(204).json(group);
};

export const deleteGroupById = async (
  request: Request<{ id: number }>,
  response: Response
) => {
  const { id } = request.params;
  const group = await groupsService.deleteGroupById(id);
  response.status(204).json(group);
};
