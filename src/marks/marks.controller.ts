import * as marksService from './marks.service'
import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import { IMarkCreateRequest } from './types/mark-create-request.interface';

export const addMarkForStudent = async (
  request: ValidatedRequest<IMarkCreateRequest>,
  response: Response
) => {
  const mark = await marksService.addMarkForStudent(request.body);
  response.status(201).json(mark);
};

export const getMarksOfStudent = async (
  request: Request<{ student_id: number }>,
  response: Response
) => {
  const { student_id } = request.query;
  console.log(typeof student_id);
  const mark = await marksService.getMarksOfStudent(Number(student_id));
  response.status(200).json(mark);
};

export const getMarksOfCourse = async (
  request: Request<{ course_id: number }>,
  response: Response
) => {
  const { course_id } = request.query;
  console.log(typeof course_id);
  const mark = await marksService.getMarksOfCourse(Number(course_id));
  response.status(200).json(mark);
};