import { IMark } from "./mark.interface";

export interface IMarkResponse extends IMark {
    course_id: number;
    student_id: number;
    lector_id: number;
}