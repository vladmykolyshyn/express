import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IMarkResponse } from './mark.response.interface'; 

export interface IMarkCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IMarkResponse, 'id'>;
}
