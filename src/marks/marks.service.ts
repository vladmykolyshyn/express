import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { coursesRepository } from '../courses/courses.service';
import { lectorsRepository } from '../lectors/lectors.service';
import { studentsRepository } from '../students/students.service';
import { Mark } from './entities/mark.entity';
import { IMarkResponse } from './types/mark.response.interface';

export const marksRepository = AppDataSource.getRepository(Mark);

export const addMarkForStudent = async (
  addMarkForStudentSchema: Omit<IMarkResponse, 'id'>
): Promise<Mark> => {
  const student = await studentsRepository.findOne({
    where: {
      id: addMarkForStudentSchema.student_id,
    },
  });
  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  const lector = await lectorsRepository.findOne({
    where: {
      id: addMarkForStudentSchema.lector_id,
    },
  });
  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  const course = await coursesRepository.findOne({
    where: {
      id: addMarkForStudentSchema.course_id,
    },
  });
  if (!course) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  const mark = await marksRepository.findOne({
    where: {
      mark: addMarkForStudentSchema.mark,
    },
  });
  if (mark) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'This mark already exists'
    );
  }

  return marksRepository.save(addMarkForStudentSchema);
};

export const getMarksOfStudent = async (
  student_id: number
): Promise<{ courseName: string; mark: number }[]> => {
  const student = await studentsRepository.findOne({
    where: {
      id: student_id,
    },
  });

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  const marks = await marksRepository
    .createQueryBuilder('mark')
    .select(['course.name', 'mark.mark'])
    .leftJoin('mark.course', 'course')
    .where('mark.student_id = :student_id', { student_id })
    .getMany();

  return marks.map((mark) => ({
    courseName: mark.course.name,
    mark: mark.mark,
  }));
};

export const getMarksOfCourse = async (course_id: number): Promise<{courseName: string; lectorName: string;
    studentName: string;
  mark: number
}[]> => {
  
  const course = await coursesRepository.findOne({
    where: {
      id: course_id,
    },
  });

  if (!course) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  const marks = await marksRepository
    .createQueryBuilder('mark')
    .select(['course.name', 'lector.name', 'student.name', 'mark.mark'])
    .leftJoin('mark.course', 'course')
    .leftJoin('mark.lector', 'lector')
    .leftJoin('mark.student', 'student')
    .where('mark.course_id = :course_id', { course_id })
    .getMany();

  return marks.map((mark) => ({
    courseName: mark.course.name,
    lectorName: mark.lector.name,
    studentName: mark.student.name,
    mark: mark.mark,
  }));
};


