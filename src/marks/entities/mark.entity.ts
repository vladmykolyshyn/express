import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Course } from '../../courses/entities/course.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Student } from '../../students/entities/student.entity';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
  @Column({
    type: 'integer',
    nullable: false,
  })
  mark: number;

  @Column({
    type: 'integer',
    nullable: false,
  })
  lector_id: number;

  @Column({
    type: 'integer',
    nullable: false,
  })
  course_id: number;

  @Column({
    type: 'integer',
    nullable: false,
  })
  student_id: number;

  @ManyToOne(() => Course, (course) => course.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ManyToOne(() => Lector, (lector) => lector.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;

  @ManyToOne(() => Student, (student) => student.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'student_id' })
  student: Student;
}
