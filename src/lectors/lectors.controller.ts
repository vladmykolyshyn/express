import { Request, Response } from 'express';
import * as lectorsService from './lectors.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ILectorCreateRequest } from './types/lector-create-request.interface';

export const getAllLectors = async (request: Request, response: Response) => {
  const lectors = await lectorsService.getAllLectors();
  response.status(200).json(lectors);
};

export const getLectorById = async (
  request: Request<{ id: number }>,
  response: Response
) => {
  const { id } = request.params;
  const lectorWithCourses = await lectorsService.getLectorById(id);
  response.status(200).json(lectorWithCourses);
};

export const getCoursesTaughtByLector = async (
  request: Request<{ id: number }>,
  response: Response
) => {
  const { id } = request.params;
  const coursesTaughtByLector = await lectorsService.getCoursesTaughtByLector(id);
  response.status(200).json(coursesTaughtByLector);
};

export const createLector = async (
  request: ValidatedRequest<ILectorCreateRequest>,
  response: Response
) => {
  const lector = await lectorsService.createLector(request.body);
  response.status(201).json(lector);
};