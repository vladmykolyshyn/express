import { Router } from 'express';
import * as coursesController from './course.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { idParamSchema } from '../application/schemas/id-param.schema';
import { courseAddLectorId, courseCreateSchema } from './course.schema';

const router = Router();

router.get('/', controllerWrapper(coursesController.getAllCourses));

router.post(
  '/',
  validator.body(courseCreateSchema),
  controllerWrapper(coursesController.createCourse)
);

router.patch(
  '/:id/lector',
  validator.params(idParamSchema),
  validator.body(courseAddLectorId),
  controllerWrapper(coursesController.addLectorToCourse)
);

export default router;
