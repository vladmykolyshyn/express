import { Request, Response } from 'express';
import * as coursesService from './courses.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ICourseCreateRequest } from './types/course-create-request.interface';
import { ICourseAddLectorRequest } from './types/course-addlector-request.inreface';

export const getAllCourses = async (request: Request, response: Response) => {
  const courses = await coursesService.getAllCourses();
  response.status(200).json(courses);
};

export const createCourse = async (
  request: ValidatedRequest<ICourseCreateRequest>,
  response: Response
) => {
  const lector = await coursesService.createCourse(request.body);
  response.status(201).json(lector);
};

export const addLectorToCourse = async (
  request: ValidatedRequest<ICourseAddLectorRequest>,
  response: Response
) => {
  const { id } = request.params;
  const lectorAddGroup = await coursesService.addLectorToCourse(
    id,
    request.body
  );
  response.status(200).json(lectorAddGroup);
};
