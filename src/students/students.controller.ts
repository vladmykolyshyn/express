import { Request, Response } from 'express';
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentUpdateRequest } from './types/student-update-request.interface';
import { IStudentCreateRequest } from './types/student-create-request.interface';
import { IStudentAddGroupRequest } from './types/student-addgroup-request.interface';
import { Student } from './entities/student.entity';

export const getAllStudents = async (
  request: Request<{ name?: string }>,
  response: Response
) => {
  const { name } = request.query;

  let students: Student[];

  if (name) {
    students = await studentsService.getStudentsByName(String(name));
  } else {
    students = await studentsService.getAllStudents();
  }

  response.status(200).json(students);
};

export const getStudentById = async (
  request: Request<{ id: number }>,
  response: Response
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentById(id);
  response.status(200).json(student);
};

export const createStudent = async (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response
) => {
  const student = await studentsService.createStudent(request.body);
  response.status(201).json(student);
};

export const updateStudentById = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response
) => {
  const { id } = request.params;
  const student = await studentsService.updateStudentById(id, request.body);
  response.status(204).json(student);
};

export const addGroupToStudent = async (
  request: ValidatedRequest<IStudentAddGroupRequest>,
  response: Response
) => {
  const { id } = request.params;
  const studentAddGroup = await studentsService.addGroupToStudent(
    id,
    request.body
  );
  response.status(200).json(studentAddGroup);
};

export const deleteStudentById = async (
  request: Request<{ id: number }>,
  response: Response
) => {
  const { id } = request.params;
  const student = await studentsService.deleteStudentById(id);
  response.status(204).json(student);
};
