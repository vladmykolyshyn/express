import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudentResponse } from './student.response.interface';

export interface IStudentAddGroupRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Pick<IStudentResponse, 'id' | 'groupId'>;
}
