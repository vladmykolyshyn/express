import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudentResponse } from './student.response.interface';

export interface IStudentUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IStudentResponse>;
}
