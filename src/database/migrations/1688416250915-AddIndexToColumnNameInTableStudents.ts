import { MigrationInterface, QueryRunner } from "typeorm";

export class AddIndexToColumnNameInTableStudents1688416250915 implements MigrationInterface {
    name = 'AddIndexToColumnNameInTableStudents1688416250915'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX "idx_student_name" ON "students" ("name") `);
        
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."idx_student_name"`);
    }

}
