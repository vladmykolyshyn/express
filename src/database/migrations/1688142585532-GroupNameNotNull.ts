import { MigrationInterface, QueryRunner } from "typeorm";

export class GroupNameNotNull1688142585532 implements MigrationInterface {
    name = 'GroupNameNotNull1688142585532'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "groups" ALTER COLUMN "name" SET NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "groups" ALTER COLUMN "name" DROP NOT NULL`);
    }

}
